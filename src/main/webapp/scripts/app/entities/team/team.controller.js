'use strict';

angular.module('articlerApp')
    .controller('TeamController', function ($scope, Team) {
        $scope.teams = [];
        $scope.loadAll = function () {
            Team.query(function (result) {
                $scope.teams = result;
            });
        };
        $scope.loadAll();

        $scope.showUpdate = function (id) {
            Team.get({id: id}, function (result) {
                $scope.team = result;
                $('#saveTeamModal').modal('show');
            });
        };

        $scope.save = function () {
            if ($scope.team.id != null) {
                Team.update($scope.team,
                    function () {
                        $scope.refresh();
                    });
            } else {
                Team.save($scope.team,
                    function () {
                        $scope.refresh();
                    });
            }
        };

        $scope.delete = function (id) {
            Team.get({id: id}, function (result) {
                $scope.team = result;
                $('#deleteTeamConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Team.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteTeamConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $('#saveTeamModal').modal('hide');
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.team = {name: null, logo: null, id: null};
            $scope.editForm.$setPristine();
            $scope.editForm.$setUntouched();
        };
    });
