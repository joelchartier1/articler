'use strict';

angular.module('articlerApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('vote', {
                parent: 'entity',
                url: '/vote',
                data: {
                    roles: ['ROLE_ADMIN'],
                    pageTitle: 'articlerApp.vote.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/vote/votes.html',
                        controller: 'VoteController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('vote');
                        return $translate.refresh();
                    }]
                }
            })
            .state('voteDetail', {
                parent: 'entity',
                url: '/vote/:id',
                data: {
                    roles: ['ROLE_ADMIN'],
                    pageTitle: 'articlerApp.vote.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/vote/vote-detail.html',
                        controller: 'VoteDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('vote');
                        return $translate.refresh();
                    }]
                }
            });
    });
