'use strict';

angular.module('articlerApp')
    .controller('VoteDetailController', function ($scope, $stateParams, Vote) {
        $scope.vote = {};
        $scope.load = function (id) {
            Vote.get({id: id}, function (result) {
                $scope.vote = result;
            });
        };
        $scope.load($stateParams.id);
    });
