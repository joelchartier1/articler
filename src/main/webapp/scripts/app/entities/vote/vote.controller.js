'use strict';

angular.module('articlerApp')
    .controller('VoteController', function ($scope, Vote) {
        $scope.votes = [];
        $scope.loadAll = function () {
            Vote.query(function (result) {
                $scope.votes = result;
            });
        };
        $scope.loadAll();

        $scope.showUpdate = function (id) {
            Vote.get({id: id}, function (result) {
                $scope.vote = result;
                $('#saveVoteModal').modal('show');
            });
        };

        $scope.save = function () {
            if ($scope.vote.id != null) {
                Vote.update($scope.vote,
                    function () {
                        $scope.refresh();
                    });
            } else {
                Vote.save($scope.vote,
                    function () {
                        $scope.refresh();
                    });
            }
        };

        $scope.delete = function (id) {
            Vote.get({id: id}, function (result) {
                $scope.vote = result;
                $('#deleteVoteConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Vote.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteVoteConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $('#saveVoteModal').modal('hide');
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.vote = {article_id: null, user_id: null, creation_date: null, value: null, id: null};
            $scope.editForm.$setPristine();
            $scope.editForm.$setUntouched();
        };
    });
