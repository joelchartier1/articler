'use strict';

angular.module('articlerApp')
    .controller('ArticleController', function ($scope, Article, ParseLinks) {
        $scope.articles = [];
        $scope.page = 1;
        $scope.loadAll = function () {
            Article.query({page: $scope.page, per_page: 20}, function (result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                for (var i = 0; i < result.length; i++) {
                    $scope.articles.push(result[i]);
                }
            });
        };
        $scope.reset = function () {
            $scope.page = 1;
            $scope.articles = [];
            $scope.loadAll();
        };
        $scope.loadPage = function (page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.showUpdate = function (id) {
            Article.get({id: id}, function (result) {
                $scope.article = result;
                $('#saveArticleModal').modal('show');
            });
        };

        $scope.save = function () {
            if ($scope.article.id != null) {
                Article.update($scope.article,
                    function () {
                        $scope.refresh();
                    });
            } else {
                Article.save($scope.article,
                    function () {
                        $scope.refresh();
                    });
            }
        };

        $scope.delete = function (id) {
            Article.get({id: id}, function (result) {
                $scope.article = result;
                $('#deleteArticleConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Article.delete({id: id},
                function () {
                    $scope.reset();
                    $('#deleteArticleConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.refresh = function () {
            $scope.reset();
            $('#saveArticleModal').modal('hide');
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.article = {name: null, url: null, creation_date: null, user_id: null, team_id: null, id: null};
            $scope.editForm.$setPristine();
            $scope.editForm.$setUntouched();
        };
    });
