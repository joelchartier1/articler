'use strict';

angular.module('articlerApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('articles', {
                parent: 'site',
                url: '/articles',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'articlerApp.article.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/article/article.html',
                        controller: 'ArticleController'
                    }
                },
                resolve: {
                    mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('article');
                        return $translate.refresh();
                    }]
                }
            });
    });
