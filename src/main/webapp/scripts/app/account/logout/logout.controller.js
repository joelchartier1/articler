'use strict';

angular.module('articlerApp')
    .controller('LogoutController', function (Auth) {
        Auth.logout();
    });
