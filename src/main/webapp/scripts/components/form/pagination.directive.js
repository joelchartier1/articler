/* globals $ */
'use strict';

angular.module('articlerApp')
    .directive('articlerAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
