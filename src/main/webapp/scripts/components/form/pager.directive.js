/* globals $ */
'use strict';

angular.module('articlerApp')
    .directive('articlerAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
