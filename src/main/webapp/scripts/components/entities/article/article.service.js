'use strict';

angular.module('articlerApp')
    .factory('Article', function ($resource, DateUtils) {
        return $resource('api/articles/:id', {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.creation_date = DateUtils.convertDateTimeFromServer(data.creation_date);
                    return data;
                }
            },
            'update': {method: 'PUT'}
        });
    });
