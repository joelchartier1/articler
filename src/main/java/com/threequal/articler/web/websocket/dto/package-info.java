/**
 * Data Access Objects used by WebSocket services.
 */
package com.threequal.articler.web.websocket.dto;
