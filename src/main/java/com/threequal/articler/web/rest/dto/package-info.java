/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.threequal.articler.web.rest.dto;
