package com.threequal.articler.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.threequal.articler.domain.Vote;
import com.threequal.articler.repository.VoteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Vote.
 */
@RestController
@RequestMapping("/api")
public class VoteResource {

    private final Logger log = LoggerFactory.getLogger(VoteResource.class);

    @Inject
    private VoteRepository voteRepository;

    /**
     * POST  /votes -> Create a new vote.
     */
    @RequestMapping(value = "/votes",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> create(@Valid @RequestBody Vote vote) throws URISyntaxException {
        log.debug("REST request to save Vote : {}", vote);
        if (vote.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new vote cannot already have an ID").build();
        }
        voteRepository.save(vote);
        return ResponseEntity.created(new URI("/api/votes/" + vote.getId())).build();
    }

    /**
     * PUT  /votes -> Updates an existing vote.
     */
    @RequestMapping(value = "/votes",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> update(@Valid @RequestBody Vote vote) throws URISyntaxException {
        log.debug("REST request to update Vote : {}", vote);
        if (vote.getId() == null) {
            return create(vote);
        }
        voteRepository.save(vote);
        return ResponseEntity.ok().build();
    }

    /**
     * GET  /votes -> get all the votes.
     */
    @RequestMapping(value = "/votes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Vote> getAll() {
        log.debug("REST request to get all Votes");
        return voteRepository.findAll();
    }

    /**
     * GET  /votes/:id -> get the "id" vote.
     */
    @RequestMapping(value = "/votes/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Vote> get(@PathVariable String id) {
        log.debug("REST request to get Vote : {}", id);
        return Optional.ofNullable(voteRepository.findOne(id))
            .map(vote -> new ResponseEntity<>(
                vote,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /votes/:id -> delete the "id" vote.
     */
    @RequestMapping(value = "/votes/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable String id) {
        log.debug("REST request to delete Vote : {}", id);
        voteRepository.delete(id);
    }
}
