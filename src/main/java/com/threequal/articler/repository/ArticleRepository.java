package com.threequal.articler.repository;

import com.threequal.articler.domain.Article;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Article entity.
 */
public interface ArticleRepository extends MongoRepository<Article, String> {

}
