package com.threequal.articler.repository;

import com.threequal.articler.domain.Team;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Team entity.
 */
public interface TeamRepository extends MongoRepository<Team, String> {

}
