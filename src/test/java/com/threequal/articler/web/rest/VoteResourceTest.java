package com.threequal.articler.web.rest;

import com.threequal.articler.Application;
import com.threequal.articler.domain.Vote;
import com.threequal.articler.repository.VoteRepository;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the VoteResource REST controller.
 *
 * @see VoteResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class VoteResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");


    private static final Integer DEFAULT_ARTICLE_ID = 0;
    private static final Integer UPDATED_ARTICLE_ID = 1;

    private static final Integer DEFAULT_USER_ID = 0;
    private static final Integer UPDATED_USER_ID = 1;

    private static final DateTime DEFAULT_CREATION_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_CREATION_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_CREATION_DATE_STR = dateTimeFormatter.print(DEFAULT_CREATION_DATE);

    private static final Boolean DEFAULT_VALUE = false;
    private static final Boolean UPDATED_VALUE = true;

    @Inject
    private VoteRepository voteRepository;

    private MockMvc restVoteMockMvc;

    private Vote vote;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        VoteResource voteResource = new VoteResource();
        ReflectionTestUtils.setField(voteResource, "voteRepository", voteRepository);
        this.restVoteMockMvc = MockMvcBuilders.standaloneSetup(voteResource).build();
    }

    @Before
    public void initTest() {
        voteRepository.deleteAll();
        vote = new Vote();
        vote.setArticle_id(DEFAULT_ARTICLE_ID);
        vote.setUser_id(DEFAULT_USER_ID);
        vote.setCreation_date(DEFAULT_CREATION_DATE);
        vote.setValue(DEFAULT_VALUE);
    }

    @Test
    public void createVote() throws Exception {
        int databaseSizeBeforeCreate = voteRepository.findAll().size();

        // Create the Vote
        restVoteMockMvc.perform(post("/api/votes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vote)))
            .andExpect(status().isCreated());

        // Validate the Vote in the database
        List<Vote> votes = voteRepository.findAll();
        assertThat(votes).hasSize(databaseSizeBeforeCreate + 1);
        Vote testVote = votes.get(votes.size() - 1);
        assertThat(testVote.getArticle_id()).isEqualTo(DEFAULT_ARTICLE_ID);
        assertThat(testVote.getUser_id()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testVote.getCreation_date().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testVote.getValue()).isEqualTo(DEFAULT_VALUE);
    }

    @Test
    public void checkArticle_idIsRequired() throws Exception {
        // Validate the database is empty
        assertThat(voteRepository.findAll()).hasSize(0);
        // set the field null
        vote.setArticle_id(null);

        // Create the Vote, which fails.
        restVoteMockMvc.perform(post("/api/votes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vote)))
            .andExpect(status().isBadRequest());

        // Validate the database is still empty
        List<Vote> votes = voteRepository.findAll();
        assertThat(votes).hasSize(0);
    }

    @Test
    public void checkUser_idIsRequired() throws Exception {
        // Validate the database is empty
        assertThat(voteRepository.findAll()).hasSize(0);
        // set the field null
        vote.setUser_id(null);

        // Create the Vote, which fails.
        restVoteMockMvc.perform(post("/api/votes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vote)))
            .andExpect(status().isBadRequest());

        // Validate the database is still empty
        List<Vote> votes = voteRepository.findAll();
        assertThat(votes).hasSize(0);
    }

    @Test
    public void checkCreation_dateIsRequired() throws Exception {
        // Validate the database is empty
        assertThat(voteRepository.findAll()).hasSize(0);
        // set the field null
        vote.setCreation_date(null);

        // Create the Vote, which fails.
        restVoteMockMvc.perform(post("/api/votes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vote)))
            .andExpect(status().isBadRequest());

        // Validate the database is still empty
        List<Vote> votes = voteRepository.findAll();
        assertThat(votes).hasSize(0);
    }

    @Test
    public void getAllVotes() throws Exception {
        // Initialize the database
        voteRepository.save(vote);

        // Get all the votes
        restVoteMockMvc.perform(get("/api/votes"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vote.getId())))
            .andExpect(jsonPath("$.[*].article_id").value(hasItem(DEFAULT_ARTICLE_ID)))
            .andExpect(jsonPath("$.[*].user_id").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].creation_date").value(hasItem(DEFAULT_CREATION_DATE_STR)))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.booleanValue())));
    }

    @Test
    public void getVote() throws Exception {
        // Initialize the database
        voteRepository.save(vote);

        // Get the vote
        restVoteMockMvc.perform(get("/api/votes/{id}", vote.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(vote.getId()))
            .andExpect(jsonPath("$.article_id").value(DEFAULT_ARTICLE_ID))
            .andExpect(jsonPath("$.user_id").value(DEFAULT_USER_ID))
            .andExpect(jsonPath("$.creation_date").value(DEFAULT_CREATION_DATE_STR))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.booleanValue()));
    }

    @Test
    public void getNonExistingVote() throws Exception {
        // Get the vote
        restVoteMockMvc.perform(get("/api/votes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateVote() throws Exception {
        // Initialize the database
        voteRepository.save(vote);

        int databaseSizeBeforeUpdate = voteRepository.findAll().size();

        // Update the vote
        vote.setArticle_id(UPDATED_ARTICLE_ID);
        vote.setUser_id(UPDATED_USER_ID);
        vote.setCreation_date(UPDATED_CREATION_DATE);
        vote.setValue(UPDATED_VALUE);
        restVoteMockMvc.perform(put("/api/votes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vote)))
            .andExpect(status().isOk());

        // Validate the Vote in the database
        List<Vote> votes = voteRepository.findAll();
        assertThat(votes).hasSize(databaseSizeBeforeUpdate);
        Vote testVote = votes.get(votes.size() - 1);
        assertThat(testVote.getArticle_id()).isEqualTo(UPDATED_ARTICLE_ID);
        assertThat(testVote.getUser_id()).isEqualTo(UPDATED_USER_ID);
        assertThat(testVote.getCreation_date().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testVote.getValue()).isEqualTo(UPDATED_VALUE);
    }

    @Test
    public void deleteVote() throws Exception {
        // Initialize the database
        voteRepository.save(vote);

        int databaseSizeBeforeDelete = voteRepository.findAll().size();

        // Get the vote
        restVoteMockMvc.perform(delete("/api/votes/{id}", vote.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Vote> votes = voteRepository.findAll();
        assertThat(votes).hasSize(databaseSizeBeforeDelete - 1);
    }
}
