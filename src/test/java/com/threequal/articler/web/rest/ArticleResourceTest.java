package com.threequal.articler.web.rest;

import com.threequal.articler.Application;
import com.threequal.articler.domain.Article;
import com.threequal.articler.repository.ArticleRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ArticleResource REST controller.
 *
 * @see ArticleResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ArticleResourceTest {

    private static final String DEFAULT_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_NAME = "UPDATED_TEXT";
    private static final String DEFAULT_URL = "SAMPLE_TEXT";
    private static final String UPDATED_URL = "UPDATED_TEXT";

    private static final DateTime DEFAULT_CREATION_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_CREATION_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_CREATION_DATE_STR = dateTimeFormatter.print(DEFAULT_CREATION_DATE);

    private static final Integer DEFAULT_USER_ID = 0;
    private static final Integer UPDATED_USER_ID = 1;

    private static final Integer DEFAULT_TEAM_ID = 0;
    private static final Integer UPDATED_TEAM_ID = 1;

    @Inject
    private ArticleRepository articleRepository;

    private MockMvc restArticleMockMvc;

    private Article article;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ArticleResource articleResource = new ArticleResource();
        ReflectionTestUtils.setField(articleResource, "articleRepository", articleRepository);
        this.restArticleMockMvc = MockMvcBuilders.standaloneSetup(articleResource).build();
    }

    @Before
    public void initTest() {
        articleRepository.deleteAll();
        article = new Article();
        article.setName(DEFAULT_NAME);
        article.setUrl(DEFAULT_URL);
        article.setCreation_date(DEFAULT_CREATION_DATE);
        article.setUser_id(DEFAULT_USER_ID);
        article.setTeam_id(DEFAULT_TEAM_ID);
    }

    @Test
    public void createArticle() throws Exception {
        int databaseSizeBeforeCreate = articleRepository.findAll().size();

        // Create the Article
        restArticleMockMvc.perform(post("/api/articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(article)))
            .andExpect(status().isCreated());

        // Validate the Article in the database
        List<Article> articles = articleRepository.findAll();
        assertThat(articles).hasSize(databaseSizeBeforeCreate + 1);
        Article testArticle = articles.get(articles.size() - 1);
        assertThat(testArticle.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testArticle.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testArticle.getCreation_date().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testArticle.getUser_id()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testArticle.getTeam_id()).isEqualTo(DEFAULT_TEAM_ID);
    }

    @Test
    public void checkNameIsRequired() throws Exception {
        // Validate the database is empty
        assertThat(articleRepository.findAll()).hasSize(0);
        // set the field null
        article.setName(null);

        // Create the Article, which fails.
        restArticleMockMvc.perform(post("/api/articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(article)))
            .andExpect(status().isBadRequest());

        // Validate the database is still empty
        List<Article> articles = articleRepository.findAll();
        assertThat(articles).hasSize(0);
    }

    @Test
    public void checkUrlIsRequired() throws Exception {
        // Validate the database is empty
        assertThat(articleRepository.findAll()).hasSize(0);
        // set the field null
        article.setUrl(null);

        // Create the Article, which fails.
        restArticleMockMvc.perform(post("/api/articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(article)))
            .andExpect(status().isBadRequest());

        // Validate the database is still empty
        List<Article> articles = articleRepository.findAll();
        assertThat(articles).hasSize(0);
    }

    @Test
    public void checkCreation_dateIsRequired() throws Exception {
        // Validate the database is empty
        assertThat(articleRepository.findAll()).hasSize(0);
        // set the field null
        article.setCreation_date(null);

        // Create the Article, which fails.
        restArticleMockMvc.perform(post("/api/articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(article)))
            .andExpect(status().isBadRequest());

        // Validate the database is still empty
        List<Article> articles = articleRepository.findAll();
        assertThat(articles).hasSize(0);
    }

    @Test
    public void checkUser_idIsRequired() throws Exception {
        // Validate the database is empty
        assertThat(articleRepository.findAll()).hasSize(0);
        // set the field null
        article.setUser_id(null);

        // Create the Article, which fails.
        restArticleMockMvc.perform(post("/api/articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(article)))
            .andExpect(status().isBadRequest());

        // Validate the database is still empty
        List<Article> articles = articleRepository.findAll();
        assertThat(articles).hasSize(0);
    }

    @Test
    public void checkTeam_idIsRequired() throws Exception {
        // Validate the database is empty
        assertThat(articleRepository.findAll()).hasSize(0);
        // set the field null
        article.setTeam_id(null);

        // Create the Article, which fails.
        restArticleMockMvc.perform(post("/api/articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(article)))
            .andExpect(status().isBadRequest());

        // Validate the database is still empty
        List<Article> articles = articleRepository.findAll();
        assertThat(articles).hasSize(0);
    }

    @Test
    public void getAllArticles() throws Exception {
        // Initialize the database
        articleRepository.save(article);

        // Get all the articles
        restArticleMockMvc.perform(get("/api/articles"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(article.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].creation_date").value(hasItem(DEFAULT_CREATION_DATE_STR)))
            .andExpect(jsonPath("$.[*].user_id").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].team_id").value(hasItem(DEFAULT_TEAM_ID)));
    }

    @Test
    public void getArticle() throws Exception {
        // Initialize the database
        articleRepository.save(article);

        // Get the article
        restArticleMockMvc.perform(get("/api/articles/{id}", article.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(article.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.creation_date").value(DEFAULT_CREATION_DATE_STR))
            .andExpect(jsonPath("$.user_id").value(DEFAULT_USER_ID))
            .andExpect(jsonPath("$.team_id").value(DEFAULT_TEAM_ID));
    }

    @Test
    public void getNonExistingArticle() throws Exception {
        // Get the article
        restArticleMockMvc.perform(get("/api/articles/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateArticle() throws Exception {
        // Initialize the database
        articleRepository.save(article);

        int databaseSizeBeforeUpdate = articleRepository.findAll().size();

        // Update the article
        article.setName(UPDATED_NAME);
        article.setUrl(UPDATED_URL);
        article.setCreation_date(UPDATED_CREATION_DATE);
        article.setUser_id(UPDATED_USER_ID);
        article.setTeam_id(UPDATED_TEAM_ID);
        restArticleMockMvc.perform(put("/api/articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(article)))
            .andExpect(status().isOk());

        // Validate the Article in the database
        List<Article> articles = articleRepository.findAll();
        assertThat(articles).hasSize(databaseSizeBeforeUpdate);
        Article testArticle = articles.get(articles.size() - 1);
        assertThat(testArticle.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testArticle.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testArticle.getCreation_date().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testArticle.getUser_id()).isEqualTo(UPDATED_USER_ID);
        assertThat(testArticle.getTeam_id()).isEqualTo(UPDATED_TEAM_ID);
    }

    @Test
    public void deleteArticle() throws Exception {
        // Initialize the database
        articleRepository.save(article);

        int databaseSizeBeforeDelete = articleRepository.findAll().size();

        // Get the article
        restArticleMockMvc.perform(delete("/api/articles/{id}", article.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Article> articles = articleRepository.findAll();
        assertThat(articles).hasSize(databaseSizeBeforeDelete - 1);
    }
}
